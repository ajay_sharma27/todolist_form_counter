import { useForm } from "react-hook-form";
import { useState } from 'react';
export default function MyForm(style: any) {
    const { register, handleSubmit, formState: { errors } } = useForm();
    const onSubmit = (data: any) => console.log(data);
    const [count, setCount] = useState(0);
    return (
        <div className="p-4 mb-2 text-center">
            <p className="h1">Form</p>
            <form onSubmit={handleSubmit(onSubmit)} className="py-3 ">
                <div className="row mb-3 justify-content-center align-items-center">
                    <label htmlFor="firstName" className="col-sm-1 col-form-label-lg ">Name:</label>
                    <div className="col-sm-4">
                        <input
                            type="name" className="form-control "
                            id="firstName"
                            aria-invalid={errors.firstName ? "true" : "false"}
                            {...register('firstName', { required: true })}
                        />
                        {errors.firstName && (
                            <span role="alert">
                                This field is required
                            </span>
                        )}
                    </div>
                </div>
                <div className="row mb-3 justify-content-center align-items-center">
                    <label htmlFor="email" className="col-sm-1 col-form-label-lg ">Email:</label>
                    <div className="col-sm-4">
                        <input
                            type="email" className="form-control "
                            id="email"
                            aria-invalid={errors.email ? "true" : "false"}
                            {...register('email', { required: true })}
                        />
                        {errors.email && (
                            <span role="alert">
                                This field is required
                            </span>
                        )}
                    </div>
                </div>
                <div className="row mb-3 justify-content-center align-items-center">
                    <label htmlFor="address" className="col-sm-1 col-form-label-lg ">Address:</label>
                    <div className="col-sm-4">
                        <input
                            type="address" className="form-control "
                            id="address"
                            aria-invalid={errors.address ? "true" : "false"}
                            {...register('address', { required: true })}
                        />
                        {errors.address && (
                            <span role="alert">
                                This field is required
                            </span>
                        )}
                    </div>
                </div><br />
                <div >
                    <input type="submit" />

                </div>
            </form><hr /><br />
            <div >
                <p className="h1">Counter</p>
                <p>You clicked {count} times</p>

                <button className="btn btn-danger py-2" onClick={() => setCount(count - 1)}>
                    -
      </button>{' '}
                <button className="btn btn-success py-2" onClick={() => setCount(count + 1)}>
                    +
      </button>
            </div>
        </div>
    );
}