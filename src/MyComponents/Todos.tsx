import { TodoItem } from "./TodoItem";
export const Todos = (props: any) => {
    let mystyle = {
        minHeight: '70vh',
        margin: '40px auto'
    }
    return (
        <div className="container" style={mystyle}>
            <h3 className=" my-3">Todos List</h3>
            {props.todos.map((todo: any) => {
                return (<TodoItem todo={todo} key={todo.sno} onDelete={props.onDelete} onEdit={props.onEdit} />
                )
            })}
        </div>
    )
}